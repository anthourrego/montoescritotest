package prueba.veritran.net.pruebauniversidad;

import java.util.HashMap;
import java.util.Map;

public class MontoEscrito {
    private static Map<Integer,String> parseoNumeros = new HashMap<Integer,String>();

    public static void main(String[] args) {
        //Anthony Smidh Urrego Pineda - Programación de Software - 4 Semestre
        System.out.println(MontoEscrito.getMontoEscrito(700));
    }


    public static String getMontoEscrito(Integer valor){
        String resultado = "";

        crearParseNumeros();
        //Millones
        if ((valor/1000000) > 0){ //Validamos que se encuentre en el rango de millones
            if((valor/1000000) == 1){ //verificamos que el numero se este en el rango de un millon
                //Se valida que si el numero se encuentra en la lista de constantes pre definidas
                if(parseoNumeros.containsKey(valor)){
                    resultado = parseoNumeros.get(valor);
                }else{
                    resultado = parseoNumeros.get(1000000) + " " + getMontoEscrito(valor%1000000);
                }
            }else{
                //Si el valor es cerrado cerrara el ciclo
                if((valor%100000) == 0){
                    resultado = getMontoEscrito(valor/1000000) + " millones";
                }else {
                    resultado = getMontoEscrito(valor/1000000) + " millones " + getMontoEscrito(valor%1000000);
                }
            }
            //Miles
        }else if((valor/1000) > 0){ //Validamos que se enceuntre en el rago de mil
            if ((valor/1000) == 1){
                //Se valida que si el numero se encuentra en la lista de constantes pre definidas
                if(parseoNumeros.containsKey(valor)){
                    resultado = parseoNumeros.get(valor);
                }else{
                    resultado = parseoNumeros.get(1000) + " " + getMontoEscrito(valor%1000);
                }
            }else{
                //Si el valor es cerrado cerrara el ciclo de lo contrario seguira buscando más números
                if((valor%1000) == 0){
                    resultado = getMontoEscrito(valor/1000) + " " + parseoNumeros.get(1000);
                }else {
                    resultado = getMontoEscrito(valor/1000) + " " + parseoNumeros.get(1000) + " " + getMontoEscrito(valor%1000);
                }
            }
            //Centenas
        } else if ((valor/100) > 0){ //Validamos que se encuentre en el rango de cien
            if ((valor/100) == 1){
                //Se valida que si el numero se encuentra en la lista de constantes pre definidas
                if(parseoNumeros.containsKey(valor)){
                    resultado = parseoNumeros.get(valor);
                }else{
                    resultado = "ciento " + getMontoEscrito(valor%100);
                }
                //Validamos si el número esta entrelos quiniento y novecientos ya que para ello se debe hacer unas validaciones especiales debido como se nombran
            }else if((valor/100) == 5 || (valor/100) == 7 || (valor/100) == 9) {
                if ((valor%100) == 0){
                    resultado = parseoNumeros.get((valor/100)*100);
                }else{
                    resultado = parseoNumeros.get((valor/100)*100) + " " + getMontoEscrito(valor%100);
                }
            }else {
                if ((valor%100) == 0){
                    resultado = parseoNumeros.get(valor/100) + "cientos ";
                }else{
                    resultado = parseoNumeros.get(valor/100) + "cientos " + getMontoEscrito(valor%100);
                }
            }
            // Decenas
        }else if ((valor/10) > 0){ //validamos que este en decenas
            //Se crea un switch para los numeros desde 10 a 20 ya que se nombran diferente de resto se llama normal
            switch (valor/10){
                case 1:
                    if(parseoNumeros.containsKey(valor)){
                        resultado = parseoNumeros.get(valor);
                    }else{
                        resultado = "dieci" + getMontoEscrito((valor%10));
                    }
                    break;
                case 2:
                    if(parseoNumeros.containsKey(valor)){
                        resultado = parseoNumeros.get(valor);
                    }else{
                        resultado = "veinti" + getMontoEscrito((valor%10));
                    }
                    break;
                default:
                    if(parseoNumeros.containsKey(valor)){
                        resultado = parseoNumeros.get(valor);
                    }else{
                        resultado = parseoNumeros.get((valor/10)*10) + " y " + getMontoEscrito((valor%10));
                    }
                    break;
            }
        }else{
            //Si no aplico en la anteriores condiciones es que es un numero de un solo digito simplemente se trae el valor
            if (parseoNumeros.containsKey(valor)){
                resultado = parseoNumeros.get(valor);
            }
        }

        return resultado;
    }

    private static void crearParseNumeros() {

        parseoNumeros.put(0,"cero");
        parseoNumeros.put(1,"uno");
        parseoNumeros.put(2,"dos");
        parseoNumeros.put(3,"tres");
        parseoNumeros.put(4,"cuatro");
        parseoNumeros.put(5,"cinco");
        parseoNumeros.put(6,"seis");
        parseoNumeros.put(7,"siete");
        parseoNumeros.put(8,"ocho");
        parseoNumeros.put(9,"nueve");
        parseoNumeros.put(10, "diez");
        parseoNumeros.put(11, "once");
        parseoNumeros.put(12, "doce");
        parseoNumeros.put(13, "trece");
        parseoNumeros.put(14, "catorce");
        parseoNumeros.put(15, "quince");
        parseoNumeros.put(20, "veinte");
        parseoNumeros.put(30, "treinta");
        parseoNumeros.put(40, "cuarenta");
        parseoNumeros.put(50, "cincuenta");
        parseoNumeros.put(60, "sesenta");
        parseoNumeros.put(70, "setenta");
        parseoNumeros.put(80, "ochenta");
        parseoNumeros.put(90, "noventa");
        parseoNumeros.put(100, "cien");
        parseoNumeros.put(500, "quinientos");
        parseoNumeros.put(700, "setecientos");
        parseoNumeros.put(900, "novecientos");
        parseoNumeros.put(1000, "mil");
        parseoNumeros.put(1000000, "un millon");
    }
}
