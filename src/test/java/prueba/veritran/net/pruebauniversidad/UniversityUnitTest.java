package prueba.veritran.net.pruebauniversidad;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class UniversityUnitTest {
    @Test
    public void ceroTest() {
        assertEquals( "cero", MontoEscrito.getMontoEscrito(0));
    }

    @Test
    public void nueveTest() {
        assertEquals( "nueve", MontoEscrito.getMontoEscrito(9));
    }

    @Test
    public void milTest() {
        assertEquals("mil", MontoEscrito.getMontoEscrito(1000));
    }

    @Test
    public void mil_1_Test() {
        assertEquals("nueve mil ciento cincuenta y seis", MontoEscrito.getMontoEscrito(9156));
    }

    @Test
    public void millonTest() {
        assertEquals("un millon", MontoEscrito.getMontoEscrito(1000000));
    }

    @Test
    public void millon_1_Test() {
        assertEquals( "tres millones doscientos noventa mil seiscientos cuarenta y uno", MontoEscrito.getMontoEscrito(3290641));
    }

    @Test
    public void millon_2_Test() {
        assertEquals( "novecientos noventa y nueve millones novecientos noventa y nueve mil novecientos noventa y nueve", MontoEscrito.getMontoEscrito(999999999));
    }

    @Test
    public void millon_3_Test() {
        assertEquals( "siete millones cuatrocientos tres", MontoEscrito.getMontoEscrito(7000403));
    }

    @Test
    public void millon_4_Test() {
        assertEquals( "ciento dos millones cuatrocientos diez mil ochocientos dos", MontoEscrito.getMontoEscrito(102410802));
    }

    @Test
    public void mllon_5_Test() {
        assertEquals( "novecientos millones diecisiete mil quinientos cuarenta y uno", MontoEscrito.getMontoEscrito(900017541));
    }



}